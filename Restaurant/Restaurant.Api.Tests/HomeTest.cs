﻿using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Restaurant.Api.Tests
{
    public class HomeTest
    {
        [Fact]
        public async Task HomeIsOk()
        {
            using var factory = new WebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var response = await client
                .GetAsync(new Uri(":",UriKind.Relative))
                .ConfigureAwait(false);

            Assert.True(response.IsSuccessStatusCode,
                $"Actual status code : {response.StatusCode}");
        }
    }
}